# StarsAndFans
 
La landing page creada se enfoca en actores y actrices de cine. Está dividida en 5 secciones principales:
 
--> Home: Pantalla de inicio
--> More Info: Muestra algunas de las principales funcionalidades del sitio
--> About us: Da información sobre la creación de la página, junto con un video promocional (en este caso es un video cualquiera)
--> Our Stars: Muestra el catálogo de estrellas de la página.
--> Reviews: Muestra los distintos comentarios dejados por los usuarios de la página. Es para mostrar que los mismos están felices de usarla.
 
Las distintas secciones están todas en la misma página, que se puede navegar fácilmente usando la barra de navegación incluida en el inicio de la página.
