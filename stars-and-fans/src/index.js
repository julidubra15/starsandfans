import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Dashboard from './components/Dashboard';
import AboutUs from './components/AboutUs';
import StarCatalogue from './components/StarCatalogue';
import Preview from './components/Preview';
import Reviews from './components/Reviews';

ReactDOM.render(
  <React.StrictMode>
    <div className="div-background">
      <div className="div-filter">
        <Dashboard />
        <Preview /> 
        <AboutUs />
        <StarCatalogue/>
        <Reviews />
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

