import React from 'react';
import List from './List'
import {Container, Row, Col} from 'reactstrap'

import "./../assets/css/Review.css"

const elements = [
    {
      image: './fan1.jfif',
      title: 'Jane Doe',
      text: 'This is incredible! I feel like I can send messages to some of my favorite actors, just like I would to an old friend!'
    },
    {
        image: './fan2.png',
        title: 'Tom Martin',
        text: 'This platform is very practical. I can follow the activity of my favourite stars, to see what projects they are working on. I can get all my information, very quickly, by just looking at my feed!'
    },
    {
        image: './fan3.jpg',
        title: 'John Smith',
        text: 'It is so cool to be able to watch such famous people live, and to be able to interact with them so quickly!'
    },
    {
        image: './fan4.png',
        title: 'Kate White',
        text: 'I cannot belive how famous some of the people on here are! Some real A-listers! This page has a lot of cool raffles that have amazing prizes! I won some really cool movie set swagger!'
    },
    {
        image: './fan5.jfif',
        title: 'Mark Cooper',
        text: 'This is GREAT!!! Some really cool videos on this site!'
    },
    {
        image: './fan6.png',
        title: 'Lucy Brown',
        text: 'I just met my favorite actor through this page!!! I cannot belive how easy it is to interact with people on here'
    },
];

function StarCatalogue(){
    return (
        <Container  id='reviews' className="review-container">
        <Container className="review-filter">
        <Row>
            <Col className="review-box">
                <Container className="review-box">
                    <div className="review-title">
                        <span className="review-title-text">Join</span>
                        <span className="review-title-text-color">&nbsp;Us!</span>
                    </div>
                    <div className="review-text">
                        <span>Many fans are already having fun!</span>
                    </div>
                </Container>
            </Col>
            <Col>
                <Container className="list-container">
                    <List elements={elements}/>
                </Container>
            </Col>
        </Row>
        </Container>
        </Container>
    );
}

export default StarCatalogue;