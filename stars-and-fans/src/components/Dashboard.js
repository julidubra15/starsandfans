import React, { useEffect, useState } from 'react';
import {AppBar,Toolbar, IconButton, Fade} from '@mui/material';
import {AiOutlineArrowDown} from 'react-icons/ai';
import NavBar from './Navbar';
import {Container, Row, Col} from 'reactstrap'

import './../assets/css/Dashboard.css';

function Dashboard (){
    const [checked, setChecked] = useState(false);
    
    useEffect(() => {
        setChecked(true);
    }, []);


    return (
        <Container id="home" className="dashboard-container">
            <AppBar style={{ background:'#170a04', boxShadow: 'none', display:'flex',flexDirection:'row'}}>
                <Toolbar>
                    <NavBar />
                </Toolbar>
            </AppBar>
            <Row>
                <Col>
                    <Fade in={checked}
                    {...(checked ? { timeout: 1000 } : {})} collapsedSize={30}>
                        <div className="dashboard-text">
                            <div className="dashboard-text-font">
                                Connect with your favorite movie stars!
                            </div>
                            <IconButton href='#preview'>
                            <h1><AiOutlineArrowDown style={{color:"white"}}/></h1>
                            </IconButton>
                        </div>
                    </Fade>
                </Col>    
            </Row>
        </Container>
    );
}

export default Dashboard