import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import CardStepper from './CardStepper';

import './../assets/css/StarCatalogue.css';

const elements = [
    {
      image: './actor1.jpg',
      alt: 'actor1',
      title: 'Florence Pugh',
      text: 'Florence Pugh is a British actress whose international breakthrough came in 2019 with her portrayals of professional wrestler Paige in the biographical sports film Fighting with My Family, a despondent American woman in the horror film Midsommar, and Amy March in the period drama Little Women.'
    },
    {
        image: './actor2.jpg',
        alt: 'actor2',
        title: 'Keanu Reeves',
        text: 'Keanu Reeves is a Canadian actor. Some of his most known films are The Matrix, John Wick, Constantine or Speed.'
    },
    {
        image: './actor3.jpg',
        alt: 'actor3',
        title: 'Will Smith',
        text: 'Will Smith is an American actor, rapper, and film producer. In 1990, his popularity increased dramatically when he began starring in the NBC television series The Fresh Prince of Bel-Air, which ran for six seasons until 1996. After the series ended, Smith transitioned from television to film and went on to star in numerous blockbuster films.'
    },
    {
        image: './actor4.jpg',
        alt: 'actor4',
        title: 'Kate Winslet',
        text: 'Kate Winslet is a British actress. She is known for her work in independent films, particularly period dramas, and usually portrays headstrong, complicated women. She is known for films like Heavenly Creatures, Titanic, Sense and Sensibility or Eternal Sunshine of the Spotless Mind.'
    },
    {
        image: './actor5.jpg',
        alt: 'actor 5',
        title: 'Steve Martin',
        text: 'Steve Martin is an American actor, comedian, writer, producer, and musician. In the 1980s, Martin became a successful actor, starring in such films as The Jerk, Three Amigos, Little Shop of Horrors, Roxanne. He has also starred as the family patriarch in Parenthood, the Father of the Bride films, and the Cheaper by the Dozen films.'
    }
];

function StarCatalogue(){
    return (
        <Container id='catalogue' className="catalogue-container">
        <Container className="catalogue-filter">
        <Row>
            <Col className="catalogue-box">
                <Container className="catalogue-box">
                    <div className="catalogue-title">
                        <span className="catalogue-title-text">Our</span>
                        <span className="catalogue-title-text-color">&nbsp;Stars</span>
                    </div>
                    <div className="catalogue-text">
                        <span>Get to know some of the stars on our platform</span>
                    </div>
                </Container>
            </Col>
            <Col>
            <div className="stepper-container">
                <CardStepper elements={elements}/>
            </div>
            </Col>
        </Row>
        </Container>
        </Container>
    );
}

export default StarCatalogue;