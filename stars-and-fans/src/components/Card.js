import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

export default function DisplayCard(props) {
  return (
    <Card sx={{color:'black', backgroundColor:'black', height:'70vh'}}>
        <CardMedia
          component="img"
          height="350"
          image={props.image}
          alt={props.alt}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" style={{color:'white'}}>
            {props.title}
          </Typography>
          <Typography variant="body2" color="text.secondary" style={{color:'white'}}>
          {props.text}
          </Typography>
        </CardContent>
    </Card>
  );
}