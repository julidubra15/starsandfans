import React from 'react';
import {Button} from '@mui/material';
import { Navbar, Container, Nav} from 'react-bootstrap';
import {AiFillWechat, AiFillHome, AiFillVideoCamera, AiFillStar, AiFillInfoCircle} from 'react-icons/ai';

import './../assets/css/Navbar.css';

function NavBar() {

    function refreshPage() {
        window.location.reload(false);
    }
    
    return (
        <Navbar expand="lg">
        <Container>
            <Navbar.Toggle style={{backgroundColor:'white'}} aria-controls="basic-navbar-nav"/>
            <Navbar.Brand href="#home">
                <div className="page-name">
                    <img src={'/star.png'} alt="Star" style={{blockSize:'5vw'}} onClick={refreshPage}></img>
                    <span className="top-bar-text">Star</span>
                    <span className="top-bar-text-color">Meet</span>
                </div>
            </Navbar.Brand>
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Nav.Link href="#home">
                    <div className="menu-item"> 
                        <Button style={{color:"white"}}>
                            <h1><AiFillHome /></h1>
                            Home
                        </Button>
                    </div>
                </Nav.Link>
                <Nav.Link href="#preview">
                    <div className="menu-item"> 
                        <Button style={{color:"white"}}>
                            <h1><AiFillInfoCircle /></h1>
                            More Info
                        </Button>
                    </div>
                </Nav.Link>
                <Nav.Link href="#video">
                    <div className="menu-item"> 
                        <Button style={{color:"white",display:"flex"}}>
                            <h1><AiFillVideoCamera /></h1>
                            About Us 
                        </Button>
                    </div>
                </Nav.Link>
                <Nav.Link href="#catalogue">
                    <div className="menu-item"> 
                        <Button style={{color:"white"}}>
                            <h1><AiFillStar /></h1>
                            Our Stars
                        </Button>
                    </div>
                </Nav.Link>
                <Nav.Link href="#reviews">
                    <div className="menu-item"> 
                        <Button style={{color:"white"}}>
                            <h1><AiFillWechat /></h1>
                            Reviews
                        </Button>
                    </div>   
                </Nav.Link>
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    );
}

export default NavBar