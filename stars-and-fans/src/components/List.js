import React from 'react'
import { Image, List } from 'semantic-ui-react'
import {Paper} from '@mui/material';
import 'semantic-ui-css/semantic.min.css';

function ListExampleAnimated(props){
    return (
    <Paper style={{maxHeight: 500, overflow: 'auto'}}>
    <List animated verticalAlign='middle' style={{background:'black'}}>
    {props.elements.map((step, index) => (
        <div>
            <List.Item style={{paddingBottom:'5%',paddingTop:'5%', paddingLeft:'5%' }}>
            <Image avatar src={step.image} />
            <List.Content>
            <List.Header style={{color:'black'}}>{step.title}</List.Header>
            <List.Description style={{color:'white'}}>{step.text}</List.Description>
            </List.Content>
            </List.Item>
        </div>
    ))}
  </List>
  </Paper>
    );
}

export default ListExampleAnimated