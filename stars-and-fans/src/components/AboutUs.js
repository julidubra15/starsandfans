import React from 'react';
import {Container, Row, Col} from 'reactstrap'
import ReactPlayer from 'react-player'

import './../assets/css/AboutUs.css';

function AboutUs(){
    return (
        <Container  id='video' className='about-us-container'>
            <Container className='about-us-filter'>
            <Row>
                <Col className="about-us-box">
                    <Container className="about-us-box">
                        <Container className="about-us-title">
                                <span className="about-us-title-text">About</span>
                                <span className="about-us-title-text-color">&nbsp;Us</span>
                        </Container>
                        <Container  className="about-us-text">
                                <span>StarMeet was created by a group of film enthusiasts, who wanted to meet the stars they admire the most in their favorite movies. They though every fan could enjoy the opportunity to chat with their favorite actors, and now many recognized performers are active and frequent participants of the platform. Join us and meet a star today!</span>
                        </Container>
                    </Container>
                </Col>  
                <Col>
                    <Container className="about-us-box">
                        <ReactPlayer url='https://www.youtube.com/watch?v=xVWCM892Dh4&ab_channel=CelebrityCompilations' height='60vh' width='100%'/>
                    </Container>
                </Col>  
            </Row>
            </Container>
        </Container>
    );
}

export default AboutUs;