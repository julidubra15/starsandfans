import React from 'react';
import {Container, Row, Col} from 'reactstrap'
import Card from './Card'

import "./../assets/css/Preview.css"


function Preview(){
    return (
        <Container id='preview' className="preview-container">
            <Row>
                <Col className="preview-col">
                    <Card
                    	image= './preview1.jpg'
                        alt= 'preview1'
                        title= 'Follow actors and actresses'
                        text= ''
                    />
                </Col>
                <Col className="preview-col">
                    <Card
                    	image= './preview2.jpeg'
                        alt= 'preview2'
                        title= 'Interact on live streams, posts and more'
                        text= ''
                    />
                </Col>
                <Col className="preview-col">
                    <Card
                    	image= './preview3.jpg'
                        alt= 'preview3'
                        title= 'Participate in raffles, win prizes'
                        text= ''
                    />
                </Col>
            </Row>
        </Container>
    );
}

export default Preview